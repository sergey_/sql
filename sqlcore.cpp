#include "sqlcore.h"
#include <QDebug>

SQLCore::SQLCore(QObject *parent) :
    QObject(parent)
{
}

// �������. ���������� �� �������� ����� �������
bool SQLCore::create(QString command) {


    QString tableName; // ��� ����� �������
    QString columnsStr; // ������, �������� �������� ������� � ����� �������

    // ������� ������� �������
    int tablePos = command.indexOf("TABLE") + 6; // ���������� ������� ������ ��������� ����� �������
    tableName = command.mid(tablePos, command.indexOf("(") - tablePos); // ���������� �������� ����� �������
    columnsStr = command.mid(command.indexOf("(") + 1, command.indexOf(")") - command.indexOf("(") - 1); // ���������� ����� � ���������� �������

    QStringList columns = columnsStr.split(","); // ���������� �������� ������� ����� �������

    // �������� DOM-��������. ��������� �������� XML.
    QDomDocument doc(tableName); // �������� XML-���������
    QDomElement  domElement = doc.createElement(tableName); // �������� ��������� �������� �������� DOM

    QDomAttr domAttr = doc.createAttribute("columns"); // �������� �������� columns, ��������� �������� �������� ���������� ����������
    domAttr.setValue(QString::number(columns.length())); // ��������� �������� ��������
    domElement.setAttributeNode(domAttr); // ������������ �������� ��������� ��������

    // ���������� �������� �����������
    for(int i = 0; i < columns.length(); ++i) {
        domAttr = doc.createAttribute("column"+QString::number(i+1)); // ������� ������� � ������ ���������
        domAttr.setValue(columns.at(i)); // ����������� ������� �������� ��������
        domElement.setAttributeNode(domAttr); // ���������� ������� ��������� ��������
    }

    doc.appendChild(domElement); // ��������� �������� ������� � XML-��������.

    // ������ XML-��������� � ����
    QFile file(tableName+".db");

    if(file.open(QIODevice::WriteOnly)) { // ��������� ���� � ������� ������ �� ������
        QTextStream(&file) << doc.toString(); // ���������� � ���� XML-����
        file.close();// ��������� ����
    }

    return true;
}

// ���������� ����� ��������� � �������
bool SQLCore::insert(QString command) {

    // ��� �������, ��������� �������, ����� ��������.
    QString tableName, columnsStr, paramsStr, copy;
    QStringList columns, params;

    //������� �������
    int intoPos = command.indexOf("INTO");
    tableName = command.mid(intoPos + 5, command.indexOf("(") - intoPos - 5); //����������� ����� �������
    columnsStr = command.mid(command.indexOf("(") + 1, command.indexOf(")") - command.indexOf("(") - 1); // ���������� ���������� ������� ������
    copy = command.mid(command.indexOf(")") + 1);
    paramsStr = copy.mid(copy.indexOf("(") + 1, copy.indexOf(")") - copy.indexOf("(") - 1); // ���������� ����� �������� ������

    columns = columnsStr.split(",");
    params = paramsStr.split(",");

    QDomDocument domDoc; // �������� DOM-���������
    QFile        file(tableName+".db"); // ����������� ����� �����, � ������� ��������� �������

    if(file.open(QIODevice::ReadOnly)) { // �������� ����� � ������� ������ �� ������
        if(domDoc.setContent(&file)) { // ������� ����� � DOM-��������
            QDomElement domElement= domDoc.documentElement(); // ��������� ��������� �������� ��������

            // ������� ����� DOM-�������
            QDomElement newElement = domDoc.createElement("element"); // �������� ������ ��������
            QDomAttr domAttr = domDoc.createAttribute("123"); // �������� ���������
            //���������� ����� ����������, ���������� � ������
            for(int i = 0; i < columns.length(); ++i) {
                domAttr = domDoc.createAttribute(columns.at(i)); // �������� �������� � ����������� ������
                domAttr.setValue(params.at(i));// ��������� ��� ��������
                newElement.setAttributeNode(domAttr);//������������ �������� ��������
            }

            // ��������� ����� ������� � DOM-��������
            domElement.appendChild(newElement);
        }
        file.close(); // ��������� ����
    } else {
        qDebug() << file.errorString();
    }

    // ���������� � ���� ����� ������
    QFile file1(tableName+".db");//����������� ����� �����
    if(file1.open(QIODevice::WriteOnly)) {//�������� ����� ������ ��� ������
        QTextStream(&file1) << domDoc.toString();//������ ����� DOM-��������� � ����
        file1.close();// �������� �����
    }

    return true;
}

/*���������� ���������� � �������*/
bool SQLCore::update(QString command) {

    QString tableName, paramName, newValue, whereParam, whereValue, copy;

    // ������� ���������� �� �������
    tableName = command.mid(command.indexOf("UPDATE") + 7, command.indexOf("SET") - command.indexOf("UPDATE") - 8);//���������� ����� �������
    paramName = command.mid(command.indexOf("SET") + 4, command.indexOf("=") - command.indexOf("SET") - 4);//���������� ������ � ���������� ��� ���������
    newValue = command.mid(command.indexOf("=") + 1, command.indexOf("WHERE") - command.indexOf("=") - 2);//���������� ������ �������� ���������
    copy = command.mid(command.indexOf("WHERE"));
    whereParam = copy.mid(copy.indexOf("WHERE") + 6, copy.indexOf("=") - copy.indexOf("WHERE") - 6);//��������� ��������� ��� �������
    whereValue = copy.mid(copy.indexOf("=") + 1, copy.indexOf(";") - copy.indexOf("=") - 1);//���������� �������� ��������� ��� �������

    QDomDocument domDoc;//�������� DOM-��������
    QFile        file(tableName+".db");//����������� ����� ����� � ��������

    // �������� ������� �� �����
    if(file.open(QIODevice::ReadOnly)) {//�������� ����� ������ �� ������
        if(domDoc.setContent(&file)) {//������� ����� � ������� DOM-��������
          QDomElement documentElement = domDoc.documentElement();//���������� �������� �������� ��������

          // ����� ������� �������� ��� ���������
          QDomNode node = documentElement.firstChild();//��������� ������� �������� �� ����
          while( !node.isNull() ) {//���� ����, ���� ������� �� ���������
            if( node.isElement() )
            {
              QDomElement element = node.toElement();//�������� ������� �������
              if(element.attribute(whereParam) == whereValue) { // ���� ��������� ��������� �� �������
                //��������� ������
                element.setAttribute(paramName, newValue);
              }
            }

            if( node.isText() )
            {
              QDomText text = node.toText();
              qDebug() << text.data();
            }

            node = node.nextSibling();//��������� ���������� �������� � ����������� �����
          }
        }
        file.close();//�������� �����
    } else {
        qDebug() << file.errorString();
    }

    // ���������� ���������
    QFile file1(tableName+".db");//����������� ����� �����
    if(file1.open(QIODevice::WriteOnly)) {//�������� ����� ������ �� ������
        QTextStream(&file1) << domDoc.toString();//������ ����� ��������� � ����
        file1.close();//�������� �����
    }

    return true;
}

/*�������� ���������� �� �������*/
bool SQLCore::_delete(QString command) {

    QString tableName, whereParam, whereValue;

    //������� �������
    tableName = command.mid(command.indexOf("FROM") + 5, command.indexOf("WHERE") - command.indexOf("FROM") - 6); // ���������� ����� �������
    whereParam = command.mid(command.indexOf("WHERE") + 6, command.indexOf("=") - command.indexOf("WHERE") - 6); // ���������� ����� ���������
    whereValue = command.mid(command.indexOf("=") + 1, command.indexOf(";") - command.indexOf("=") - 1); // ���������� �������� ���������

    //�������� ������� �� �����
    QDomDocument domDoc;//������� DOM-��������
    QFile        file(tableName+".db");//����������� ����� ����� � ��������

    //��� ��� ��, ��� � � update
    if(file.open(QIODevice::ReadOnly)) {//�������� ����� ������ �� ������
        if(domDoc.setContent(&file)) {
          QDomElement documentElement = domDoc.documentElement();

          //����� ������� ��������
          QDomNode node = documentElement.firstChild();
          while( !node.isNull() ) {
            if( node.isElement() )
            {
              QDomElement element = node.toElement();
              if(element.attribute(whereParam) == whereValue) {
                  //�������� ��������
                documentElement.removeChild(node);
              }
            }

            if( node.isText() )
            {
              QDomText text = node.toText();
              qDebug() << text.data();
            }

            node = node.nextSibling();
          }
        }
        file.close();
    } else {
        qDebug() << file.errorString();
    }

    //�������� ���������
    QFile file1(tableName+".db");
    if(file1.open(QIODevice::WriteOnly)) {
        QTextStream(&file1) << domDoc.toString();
        file1.close();
    }

    return true;
}

QString SQLCore::select(QString command) {

    QString tableName, selectedParam, whereParam, whereValue;
    QString returnedStr;

    //���������� ���� ����������
    tableName = command.mid(command.indexOf("FROM") + 5, command.indexOf("WHERE") - command.indexOf("FROM") - 6);
    selectedParam = command.mid(command.indexOf("SELECT") + 7, command.indexOf("FROM") - command.indexOf("SELECT") - 8);
    if(command.indexOf("WHERE") == -1) {
        tableName = command.mid(command.indexOf("FROM") + 5, command.indexOf(";") - command.indexOf("FROM") - 5);
        whereParam = "NO";
    } else {
        whereParam = command.mid(command.indexOf("WHERE") + 6, command.indexOf("=") - command.indexOf("WHERE") - 6);
        whereValue = command.mid(command.indexOf("=") + 1, command.indexOf(";") - command.indexOf("=") - 1);
    }

    QDomDocument domDoc;
    QFile        file(tableName+".db");

    if(file.open(QIODevice::ReadOnly)) {
        if(domDoc.setContent(&file)) {
          QDomElement documentElement = domDoc.documentElement();

          QDomNode node = documentElement.firstChild();
          while( !node.isNull() ) {
            if( node.isElement() )
            {
              QDomElement element = node.toElement();
              if(element.attribute(whereParam) == whereValue || whereParam == "NO") {
                  if(selectedParam != "*") {
                      returnedStr += element.attribute(selectedParam) + '\n';
                  } else {
                      QDomNamedNodeMap map = element.attributes();
                      for(int i = 0; i < map.length(); ++i) {
                          returnedStr += map.item(i).toAttr().name() + "=" + map.item(i).toAttr().value() + " ";
                      }
                      returnedStr += '\n';
                  }
              }
            }

            if( node.isText() )
            {
              QDomText text = node.toText();
              qDebug() << text.data();
            }

            node = node.nextSibling();
          }
        }
        file.close();
    } else {
        qDebug() << file.errorString();
    }
    return returnedStr;
}

//��������, ������������ ��� ������� � ���������� �������, �������������� ������ ������.
QString SQLCore::exec(QString command) {
    char letter = command.at(0).toAscii();//����� ������ ����� �� �������

    switch(letter) {
    case 'C': // ���� ����� C, �� ���������� �-�� create
        create(command) ? emit queryResult("OK") : emit queryResult("FAIL");
        break;
    case 'I'://���� ����� I, �� ���������� �-�� insert
        insert(command) ? emit queryResult("OK") : emit queryResult("FAIL");
        break;
    case 'S'://���� ����� S, �� ���������� �-�� select
        emit queryResult(select(command));
        break;
    case 'D'://���� ����� D, �� ���������� �-�� delete
        _delete(command) ? emit queryResult("OK") : emit queryResult("FAIL");
        break;
    case 'U'://���� ����� U, �� ���������� �-�� update
        update(command) ? emit queryResult("OK") : emit queryResult("FAIL");
        break;
    default:
        emit queryResult("FAIL");
    }

    return "OK";
}

void SQLCore::slotExec(QString str) {
    exec(str);
}
