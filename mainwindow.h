#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "sqlcore.h"
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_pushButton_clicked();

    void on_resultsEdit_textChanged();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;

    SQLCore sql;
};

#endif // MAINWINDOW_H
