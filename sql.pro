#-------------------------------------------------
#
# Project created by QtCreator 2013-01-13T21:12:26
#
#-------------------------------------------------

QT       += core gui + xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sql
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sqlcore.cpp

HEADERS  += mainwindow.h \
    sqlcore.h

FORMS    += mainwindow.ui
