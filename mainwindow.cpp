#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&sql, SIGNAL(queryResult(QString)), ui->resultsEdit, SLOT(append(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString queryText;
    queryText = ui->queryEdit->text();

    if(queryText.isEmpty()) {
        //QMessageBox::Warning(this, "������", "������� ����� �������");
        return ;
    } else {
        sql.exec(queryText);
    }

    ui->queryEdit->clear();

    ui->historyEdit->appendPlainText(queryText);
}

void MainWindow::on_resultsEdit_textChanged()
{
    //ui->resultsEdit->append("<hr>");
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->resultsEdit->clear();
    ui->historyEdit->clear();
}
