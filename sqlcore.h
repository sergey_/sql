#ifndef SQLCORE_H
#define SQLCORE_H

#include <QObject>
#include <QRegExp>
#include <QStringList>
#include <QtXml/QtXml>

/**
  *
  * SQLCore - �����, ����������� ������ � ������ ���� ������ � �������������� ������� � ��.
  * ������������ ������� �������� � ��������������� ������ � ������ ������� ������.
  * ��� ������ ������ ��������� ������ QtXml.
  * ����� ����������� �� QObject ��� ����, ��� ���������� ��������� ��������-������ Qt.
  */

class SQLCore : public QObject
{
    Q_OBJECT
public:

    /* ����������� ������. ��������� ��������� �� ������������ ������� � �������� ������� Qt */
    explicit SQLCore(QObject *parent = 0);

    /* �������, ������������� ��� ������� � ���������� �������-���������� ������ �������.*/
    QString exec(QString command);

private:

    bool create(QString command); // ���������� ����� ��������� � �������
    QString select(QString command); // ������� ���������
    bool insert(QString command); // ���������� ���������
    bool update(QString command); // ���������� ������
    bool _delete(QString command); // �������� ���������
    
signals:
    void queryResult(QString str); // ������, ������������ ����� ���������� �������. ���� � ���� ���������.
public slots:
    void slotExec(QString str); //���� ��� ���������� ��������. ��������� ������ � ��������.
};

#endif // SQLCORE_H
